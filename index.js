// ./src/index.js
// Start the local API server with command
// node index.js (have to have node.js installed)

// importing the dependencies
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const mysql = require('mysql');

// Creates the connection to the CS411 Database in our project
const con = mysql.createConnection({
    host: "database-2.cwvzshksbhpv.us-east-2.rds.amazonaws.com",
    user: "Jason",
    password: "CS411",
    database : "CS411Project"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    //con.end();
});

// defining the Express app
const app = express();

// adding Helmet to enhance your API's security
app.use(helmet());

// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());

// enabling CORS for all requests
app.use(cors());

// adding morgan to log HTTP requests
app.use(morgan('combined'));

/*
 *  Example API Get Call
 *  Calling http://localhost:1433/example will call this query
 *  Result comes back from the databse in JSON, which our app can parse and print 
 */
app.get('/example', (req, res) => {
    con.query('SELECT * FROM gpa LIMIT 10', function(err, result, fields) {
        if (err) res.send(err);
        if (result) res.send(result);
    });
    console.log()
});

/*
*   Terminate API
*   Description: call this API before the Android app to terminate database connection
*                property
*   input: NONE
*/
app.post('/terminate', (req, res) => {
    con.end(function(err) {
        if(err) throw err;
    })
    console.log("Connection to DB terminated.");
    //process.exit
});

/*
*   get-salary API
*   Description: given firstName and lastName, find the salary
*   input: firstName,lastName
*   test URL: http://localhost:1433/get-salary?firstName=Augusto&lastName=Espiritu
*/
app.get('/get-salary', (req, res) => {
    let input_string = '%' + req.query.lastName + '%' + req.query.firstName + '%';
    con.query('SELECT DISTINCT instructor, AVG(salary) FROM gpa  WHERE instructor LIKE ? GROUP BY instructor', [input_string], function(err, result, fields) {
        if (err) res.send(err);
        if (result) res.send(result);//need to check empty query on Android end
    });
});

/*
*   insert-professor-rating API
*   Description: insert the new row for a uncommented professor
*   input: rating, netid
*   test URL: http://localhost:1433/insert-professor-rating?firstName=A&lastName=E&rating=3&netid=a
*/
app.get('/insert-professor-rating', (req, res) => {
    let input_string = req.query.lastName + ', ' + req.query.firstName;
    let one=0;
    let two=0;
    let three=0;
    let four=0;
    let five=0;
    switch(Number(req.query.rating)){
        //req.query.rating should be an int from 1-5
        case 1:
            one++;
            break;
        case 2:
            two++;
            break;
        case 3:
            three++;
            break;
        case 4:
            four++;
            break;
        case 5:
            five++;
            break;
        default:
            break;
    }
    con.query('INSERT INTO professor_rating VALUES (?,?,?,?,?,?,?)', [req.query.netid,input_string,one,two,three,four,five], function(err, result, fields) {
        if (err) res.send(err);
        if (result) res.send(result);//need to check empty query on Android end
    });
});


/*
*   update-professor-rating API
*   Description: given professor's comment id and its new rating, update his rating 
*   input: netid, professor_rating
*   test URL: http://localhost:1433/update-professor-rating?professor_rating=1&netid=a
*/
app.get('/update-professor-rating', (req, res) => {
    let input_string;
    switch (Number(req.query.professor_rating)){
        case 1:
            input_string = 'one_star';
            break;
        case 2:
            input_string = 'two_star';
            break;
        case 3:
            input_string = 'three_star';
            break;
        case 4:
            input_string = 'four_star';
            break;
        case 5:
            input_string = 'five_star';
            break;
        default:
            break;//should never happen
    }
    input_string = 'UPDATE professor_rating SET ' + input_string + '=' + input_string +'+1 WHERE netid=?';
    con.query(input_string, [req.query.netid], function(err, result, fields) {
        if (err) res.send(err);
        if (result) res.send(result);
    });
});

/*
*   get-professor-ratings API
*   Description: get professors' ids and their ratings
*   input: NONE
*   test URL: http://localhost:1433/get-professor-ratings
*/
app.get('/get-professor-ratings', (req, res) => {
    con.query('SELECT * FROM professor_rating', function(err, result, fields) {
        if (err) res.send(err);
        if (result) res.send(result);//need to check empty query on Android end
    });
});

/*
*   get-professor-rating API
*   Description: get the professor's rating record by his/her id
*   input: netid
*   test URL: http://localhost:1433/get-professor-rating?netid=a
*/
app.get('/get-professor-rating', (req, res) => {
    con.query('SELECT * FROM professor_rating WHERE netid=?', [req.query.netid], function(err, result, fields) {
        if (err) res.send(err);
        if (result) res.send(result);//need to check empty query on Android end
    });
});


/*
*   delete-professor-rating API
*   Description: delete the professor's rating record by his/her id
*   input: netit
*   test URL: http://localhost:1433/delete-professor-rating?netid=a
*/
app.get('/delete-professor-rating', (req, res) => {
    con.query('DELETE FROM professor_rating WHERE netid=?', [req.query.netid], function(err, result, fields) {
        if (err) res.send(err);
        if (result) res.send(result);//need to check empty query on Android end
    });
});

/*
*   top20-gpa-courses-in-department API
*   Description: returns jsons of 20 classes of the highest GPA in given 
*                   department and level range [lower_bound,upper_bound)
*   input: course_subject, upper_bound, lower_bound
*   test URL: http://localhost:1433/top20-gpa-courses-in-department?lower_bound=0&upper_bound=400&course_subject=CS
*/
app.get('/top20-gpa-courses-in-department', (req, res) => {
    if (req.query.lower_bound == ''){req.query.lower_bound='0';}
    if (req.query.upper_bound == ''){req.query.upper_bound='600';}
    if (req.query.course_subject==''){req.query.course_subject='CS';}
    con.query('select c.course_subject, c.course_number, AVG(g.avg_gpa) as GPA from gpa g join course_catalog c on g.course_number = c.course_number and g.course_subject = c.course_subject where c.course_subject = ? and c.course_number >= ? and c.course_number < ? group by c.course_number order by GPA desc limit 20', [req.query.course_subject, Number(req.query.lower_bound), Number(req.query.upper_bound)], function(err, result, fields) {
        if (err) res.send(err);
        if (result) res.send(result);//need to check empty query on Android end
    });
});

/*
*   top-gpa-courses-with-certain-gened API
*   Description: returns jsons of all classes with the specified gened in order of descending gpa
*   input: 6 different gened types
*   test URL: http://localhost:1433/top-gpa-courses-with-certain-gened?acp=NULL&cs=YES&hum=NULL&nat=NULL&qr=NULL&sbs=NULL
*/
app.get('/top-gpa-courses-with-certain-gened', (req, res) => {
    if (req.query.acp == 'YES'){req.query.acp='';}
    if (req.query.cs == 'YES'){req.query.cs='';}
    if (req.query.hum=='YES'){req.query.hum='';}
    if (req.query.nat == 'YES'){req.query.nat='';}
    if (req.query.qr == 'YES'){req.query.qr='';}
    if (req.query.sbs=='YES'){req.query.sbs='';}
    con.query("select course_subject, course_number, avg(avg_gpa) as GPA from gened natural join gpa where gened.acp NOT LIKE ? and gened.cs NOT LIKE ? and gened.hum NOT LIKE ? and gened.nat NOT LIKE ? and gened.qr NOT LIKE ? and gened.sbs NOT LIKE ? group by course_subject, course_number order by avg(avg_gpa) desc", [req.query.acp, req.query.cs, req.query.hum, req.query.nat, req.query.qr, req.query.sbs], function(err, result, fields) {
        if (err) res.send(err);
        if (result) res.send(result);//need to check empty query on Android end
    });
});

// starting the server
app.listen(1433, () => {
  console.log('listening on port 1433');
});

//app.get('http://connectivitycheck.gstatic.com/generate_204',(req,res)=>{
//    res.status(200).send({test:"test"});
//});

// Add more api calls down here
// There are lots of posts online about customizing the calls to allow for parameters
// and perform other functions (insert/update/delete/get)